# Gouvernance du projet

## Équipes du projet

Comme décrit précédemment, le développement du Rust est encadré par un certain nombre d'équipes, chacune responsable d'une partie du projet et veillant à sa bonne conduite.

Citons-en quelques unes:
- L'équipe *Core* : Il s'agit de l'équipe qui dirige le projet de manière générale. Elle dirige également les autres équipes. Des ex-membres reconnus sont Carol Nichols et Steve Klabnik, auteurs du guide officiel du langage.
- L'équipe *Compiler* : Ses membres sont chargés du développement et de l'optimisation continue du compilateur `rustc`.
- L'équipe *Language* : Cette équipe est responsable de la conception et du développement de nouvelles fonctionnalités du langage même.
- L'équipe *Library* : Elle est chargée de maintenir la bibliothèque standard du Rust, ainsi que les bibliothèques officielles `rust-lang`.

Une liste plus complète de ces équipes et de leur composition peut être trouvée sur [la page *Governance* du site officiel](https://www.rust-lang.org/governance).

## Processus de décision

Ainsi, les équipes du projet Rust s'occupent des opérations quotidiennes et du développement de leur domaine précis.

Mais il existe également un processus réglementé au sein du projet afin de prendre les décisions majeures, telles que l'ajout de nouvelles fonctionnalités. Comme ces décisions impliquent plusieurs équipes à la fois, il est important que n'importe quel membre puisse y participer.

Le processus qui a été choisi passe par des [*Requests for Comments*](https://github.com/rust-lang/rfcs), ou RFC : toute décision est proposée sous cette forme, et est discutée publiquement par quiconque souhaite le faire, afin d'atteindre un consensus et une compréhension globale des enjeux.

Une fois adoptés, ces RFC servent de références pour les équipes du projet, qui planifient alors leur travail en fonction.

Ces RFC sont également utilisées pour établir une feuille de route annuelle, qui permet de visualiser les ambitions du projet sur le moyen terme.

