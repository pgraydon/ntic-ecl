# La communauté Rust

Comme la plupart des projets *open source*, le Rust est principalement porté par la communauté qui s'est construite autour de lui, au fur et à mesure de son développement.

Cette communauté est présente sur nombre de plateformes en ligne, parmi lesquelles on peut citer [le forum officiel des utilisateurs](https://users.rust-lang.org/), [un canal Discord](https://discord.com/invite/rust-lang) ou encore [un *subreddit* dédié](https://www.reddit.com/r/rust/). Selon ses membres, elle est réputée ouverte, accueillante, et bienveillante, des valeurs qui la démarqueraient des communautés autour d'autres langages.

Outre ces plateformes, le développement du Rust lui-même est encadré par plusieurs équipes, selon une gouvernance précise lui permettant d'assurer les niveaux de qualité attendus. C'est cette gouvernance qui sera présentée dans cette partie, accompagnée de quelques statistiques à propos de la popularité du langage, et de sa communauté au sens large.
