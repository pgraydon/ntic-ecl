# Popularité et statistiques

Bien que relativement jeune pour un langage de programmatino, Rust est aujourd'hui en plein essor, et a rapidement pris en popularité ces dernières années.

Depuis 2016, il s'agit du langage le plus apprécié par les développeurs selon le [*Stack Overflow Developer Survey*](https://insights.stackoverflow.com/survey/2021), grâce à ses nombreuses innovations et son expérience de développement agréable.

La figure ci-dessous montre les dix langages les plus appréciés lors du sondage de 2021, où chaque développeur peut classer un langage comme adoré (en bleu) ou détesté (en violet). Ce sondage introduit naturellement un biais à travers l'échantillon de développeurs visé, mais offre tout de même des informations intéressantes.

![Les langages de programmation les plus appréciés en 2021](../../images/MostLovedLanguages.png)
*Les langages de programmation les plus appréciés en 2021, d'après le Stack Overflow Developer Survey* 

En termes de popularité brute, les chiffres sont nettement plus bas, mais indiquent tout de même une popularité grandissante :
- Toujours selon le *Stack Overflow Developer Survey* 2021, Rust est le seizième langage le plus utilisé par les développeurs dans un contexte général, avec un score de 7,03% contre les 64,96% du JavaScript situé en tête.

    Dans [le sondage de 2022](https://survey.stackoverflow.co/2022/), Rust grimpe à la quatorzième place, avec un score de 9,32%, contre les 65,36% du JavaScript toujours en tête.
- Selon le réputé [indice TIOBE](https://www.tiobe.com/tiobe-index/) mesurant la popularité des langages de programmation, le Rust est rentré dans le top 20 en juin 2020.

Enfin, le Rust commence également à être adopté par de nombreuses entreprises de toutes tailles, qui déploient du code Rust en production. Nous pouvons bien sûr citer Mozilla, qui l'a vu naître, mais également AWS, Microsoft, Facebook (maintenant Meta), Discord, ou encore Dropbox, parmi d'autres...

-----

Ainsi, ces statistiques et diverses informations sont un bon présage quant à l'avenir du langage : son utilisation ne fait qu'augmenter parmi les développeurs, que ce soit dans un contexte personnel ou professionnel. Ses qualités sont également très appréciées, générant un engouement qui se retrouve et se partage au sein de sa communauté grandissante.
