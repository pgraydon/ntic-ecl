# La programmation Web

Enfin, le Rust est également adapté à la **programmation Web**, un domaine considéré comme celui du JavaScript, du TypeScript et de leurs innombrables *frameworks*.

Cette possibilité est offerte à travers l'écriture de *backends* en Rust, mais se fait surtout à travers le portage simple du Rust en [**WebAssembly**](https://webassembly.org). Ce dernier est un format d'instruction principalement destiné à être exécuté par les navigateurs Web, comme son nom l'indique.

Il est nativement supporté par tous les grands navigateurs comme Chrome, Firefox et Safari, et permet l'exécution très performante d'applications au sein de ceux-ci.

Les applications WASM ainsi générées à partir du Rust bénéficient donc de ses avantages, et interagissent naturellement avec les technologies Web existantes citées plus haut...

Encore une fois, le site officiel du langage possède [des ressources à ce sujet](https://www.rust-lang.org/what/wasm), décrivant les avantages et donnant accès à divers guides et tutoriels.
