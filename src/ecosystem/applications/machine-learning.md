# Le *machine learning*

Tout aussi étonnamment, le ***machine learning*** commence à trouver sa place dans l'écosystème Rust.

Bien qu'actuellement, la plupart des *crates* disponibles soient de simples *bindings* et *wrappers* autour des bibliothèques Python reconnues comme [PyTorch](https://pytorch.org/) ou [TensorFlow](https://www.tensorflow.org/), les développements natifs s'accélèrent dans tous les domaines.

Il existe un site Internet dédié au suivi détaillé de l'état d'avancement du *machine learning*, intitulé [Are We Learning Yet?](https://www.arewelearningyet.com/). Une capture d'écran du résumé de ce suivi est donné ci-dessous.

![Suivi de l'avancement du *machine learning* en Rust](../../images/RustMachineLearning.png)
*Suivi de l'état d'avancement du machine learning en Rust*
