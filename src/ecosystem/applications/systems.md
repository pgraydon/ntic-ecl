# La programmation système

La première catégorie d'application est sans doute celle pour laquelle le Rust est le plus adapté : **la programmation système**.

Grâce à sa performance, sa fiabilité, et sa sécurité, le Rust est très adapté au développement de composants système critiques, tels que des utilitaires réseau, des pilotes, mais également des outils en ligne de commande.

Il peut même être utilisé pour écrire des systèmes d'exploitation entiers, comme en témoigne [RedoxOS](https://www.redox-os.org/), un système d'exploitation UNIX entièrement écrit en Rust.

![Capture d'écran de RedoxOS](../../images/RedoxOS.jpeg)
*Capture d'écran de RedoxOS, un système d'exploitation UNIX à micro-noyau*

Depuis octobre 2022, le Rust est d'ailleurs le premier langage hors du C qui peut être utilisé pour écrire des composants du noyau Linux. Celui-ci est incontestablement le projet informatique le plus important au monde, sur lequel repose une grande majorité de l'infrastructure informatique mondiale : il s'agit donc d'un grand succès, qui montre encore une fois les mérites du langage.

Enfin, citons un exemple d'outil de ligne de commande. [Helix](https://www.helix-editor.com) est un éditeur de texte modal, au même titre que [Vim](https://www.vim.org), [Neovim](https://neovim.io) ou [Kakoune](https://www.kakoune.org), desquels il s'inspire fortement. Il est écrit en Rust, et amène des fonctionnalités modernes et sympathiques qui le rendent plus abordable que ces derniers.

![Capture d'écran de Helix](../../images/Helix.png)
*Capture d'écran de Helix, un éditeur de texte modal en ligne de commande*

Une liste assez exhaustive des divers outils de ligne de commande écrits en Rust peut se trouver [sur cette page GitHub](https://github.com/sts10/rust-command-line-utilities), et le site officiel du langage possède également [des ressources dédiées au développement des outils en ligne de commande](https://www.rust-lang.org/what/cli).
