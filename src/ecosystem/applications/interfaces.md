# Les interfaces graphiques

Le deuxième type d'application dans lequel le Rust est utilisé est celui des **interfaces graphiques**, bien que ceci puisse paraître surprenant.

Plusieurs bibliothèques d'interfaces graphiques ont déjà été dévelopées : une des plus matures est [Iced](https://github.com/iced-rs/iced), qui est d'ailleurs utilisé par l'entreprise System76, les créateurs de la distribution GNU/Linux [Pop!_OS](https://pop.system76.com/), pour le développement d'un nouvel environnement de bureau à destination de celui-ci intitulé COSMIC.

Ainsi, la partie graphique et la partie fonctionnelle de cet environnement de bureau seront toutes deux écrites en Rust.

![Capture d'écran de COSMIC](../../images/CosmicDE.png)
*Capture d'écran d'un prototype de COSMIC, un environnement de bureau pour Linux*
