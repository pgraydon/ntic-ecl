# Applications

Apprendre un langage de programmation, c'est bien, mais ce n'est pas un investissement très utile s'il n'est pas possible de concevoir grand chose avec.

Heureusement, le Rust est un langage *à usage général*, et en tant que tel, peut être utilisé pour construire une grande variété de projets et d'applications.

Le langage est cependant encore jeune : il subsiste des domaines dans lequel il manque encore des *crates* matures et fiables, mais au vu du nombre grandissant de développeurs expérimentés qui s'intéressent au langage, ces lacunes seront sûrement comblées dans un futur proche.

En attendant, intéressons-nous à l'existant. Dans cette dernière section, seront décrits les divers domaines d'application du Rust, à travers des exemples des différents logiciels et *frameworks* développés par la communauté.

Cette liste ne peut bien sûr être exhaustive : il y a simplement beaucoup trop de projets pour tous les lister ici. Les lecteurs intéressés par un domaine particulier ou par une exploration plus poussée sont invités à effectuer leurs propres recherches sur le sujet.
