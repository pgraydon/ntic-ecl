# Écosystème et applications

Maintenant que le langage même a été présenté, et ses innovations majeures examinées, il est temps d'élargir notre propos et de présenter les autres éléments qui se sont construits autour du Rust, et qui contribuent à offrir une expérience d'utilisation considérée comme très agréable. 

Dans cette dernière partie, il sera fait état de trois de ces éléments :
- Une communauté vibrante, allant des équipes dirigeantes du projet jusqu'aux diverses plateformes regroupant des milliers d'utilisateurs
- Les divers outils indispensables constituant l'environnement de développement Rust
- Les nombreuses applications pour lesquels il est utilisé, qui deviennent de plus en plus nombreuses
