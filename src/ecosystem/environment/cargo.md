# Cargo

Cargo est un outil en ligne de commande et le gestionnaire de paquets et de projets en Rust, qui se place au centre du flux de travail de tout développeur utilisant le langage. 

Il peut télécharger toutes les dépendances d'un projet, les construire, compiler le projet et l'exécuter. Il gère également l'exécution et la vérification de tests, la vérification du format du code, et peut inversement publier un projet sur [crates.io](https://crates.io/) en tant que nouvelle *crate*.

Pour créer un nouveau projet Rust, il suffit d'exécuter

```bash
cargo new project_name
```

et Cargo se charge du reste. Un nouveau répertoire est créé dans le répertoire courant, et contient le minimum nécessaire, à savoir :
- Un répertoire `src` contenant un simple fichier `main.rs`
- Un fichier de configuration du projet `Cargo.toml`
- Un répertoire `git`

Toute dépendence externe doit être déclarée dans la bonne section du fichier `Cargo.toml`, et Cargo se chargera de les importer automatiquement lors de la prochaine compilation.

À ce sujet, il est possible de compiler et d'exécuter des projets en une simple commande :

```bash
cargo run
```

Il existe d'ailleurs une option pour cette commande qui permet d'optimiser le code compilé, de manière similaire au C++ :

```bash
cargo run --release
```

Comme vous pouvez le voir, il existe une pléthore de commandes disponibles pour Cargo, et il est même possible d'en écrire de nouvelles et de les partager librement en ligne.

Il ne sera pas fait état ici de toutes ces commandes ici par souci de concision : pour les lecteurs intéressés, il existe à ce sujet une [documentation exhaustive](https://doc.rust-lang.org/cargo/).
