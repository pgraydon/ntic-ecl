# Les crates

Commencons par une des composantes majeures de l'écosystème d'un langage de programmation : **les bibliothèques**.

Comme d'autres langages, le Rust possède la capacité d'importer et d'utiliser des bibliothèques internes ou externes, communément appelées des *crates*.

Celles-ci sont publiées et répertoriées sur le site Internet [crates.io](https://crates.io/), et il en existe plus de 108 000 à la date d'écriture de ce document (fin mars 2023), avec plus de 29 milliards de téléchargements.

Il en existe pour tous les goûts, et s'il subsiste encore des domaines où il manque des fonctionnalités, une *crate* adaptée ne tardera sûrement pas à apparaître au vu de la popularité grandissante du langage.

Du point de vue de la pratique même du développement, ces *crates* constituent donc des dépendances externes pour un projet. Celles-ci sont gérées en Rust par l'utilitaire `cargo`, qui fait l'objet de la partie suivante.
