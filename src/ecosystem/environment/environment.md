# L'environnement de développement

Un autre aspect important dans l'utilisation et la pratique d'un langage est son environnement de développement. Si celui-ci est négligé, l'expérience de développement en pâtit et la qualité du code développé a toutes ses chances de diminuer.

Heureusement, et comme décrit précédemment, le Rust bénéficie d'une expérience de développement réputée des plus agréables, ce qui contribue largement à sa popularité grandissante. Là aussi, les développeurs du langage et la communauté en général ont cherché à résoudre les problèmes répandus dans d'autres langages de programmation, le plus saillant étant le manque d'une documentation exhaustive.

L'environnement de développement du Rust sera donc présenté dans cette partie à travers trois de ses composants majeurs : 
- Les bibliothèques du langage, appelées *crates*
- Le gestionnaire de projets Cargo
- Les aides au développement, notamment `rust-analyzer` et la documentation du langage
