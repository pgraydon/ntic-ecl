# Aides au développement

Outre Cargo, il existe de formidables outils facilitant le développement en Rust. Deux d'entre eux seront présentés ici.

## `rust-analyzer`

Le premier, et sans doute le plus apprécié, est [`rust-analyzer`](https://rust-analyzer.github.io/).

Il s'agit d'une implantation du [Language Server Protocol](https://microsoft.github.io/language-server-protocol/) pour le Rust, qui permet d'offir à votre éditeur préféré des fonctionnalités comme la complétion automatique, le retour à la définition, le formattage automatique du code, du *feedback* en direct du compilateur...

Tous les éditeurs majeurs comme Visual Studio Code, Sublime Text, Emacs et Vim supportent le protocole LSP, qui facilite grandement le développement et rend l'expérience bien plus fluide et agréable.

## Une documentation exhaustive

Un autre problème majeur (mais pas assez reconnu !) que les développeurs du Rust ont voulu résoudre est le manque d'une documentation complète, exhaustive et précise.

Ainsi, c'est exactement ce qui a été écrit pour le langage, et une copie de [cette documentation](https://www.rust-lang.org/learn) est d'ailleurs incluse dans toute installation de Rust, afin de pouvoir être lue facilement et localement sur votre ordinateur.

De plus, il est aisé d'écrire et de générer de la documentation pour un projet, que ce soit en vue d'une publication en tant que *crate* ou bien pour une base de code en entreprise : Cargo a la faculté de générer une page HTML contenant toute la documentation d'un projet et d'ouvrir le résultat dans un navigateur Web.  L'exécution des tests du projet via la commande `cargo test` vérifie même les tests présents dans les *docstrings* ! 

Enfin, la documentation des différentes *crates* est également disponible sur le site Internet [docs.rs](https://docs.rs/).
