# Références

Cette section annexe liste les diverses références qui m'ont servi durant mes recherches. Elles sont organisées selon la structure du rapport.

De manière générale, toutes ces références sont complémentées par mon expérience personnelle avec le langage, ainsi que par les informations répétées par plusieurs sources que j'ai pu lire et entendre depuis environ un an.

Deux autres sources qui m'ont servi à ces fins sont le journal hebdomadaire [This Week in Rust](https://this-week-in-rust.org/), qui compile divers articles et informations en ligne sur le sujet du Rust parus durant la dernière semaine, ainsi que divers articles sur le site [Hacker News](https://news.ycombinator.com/).


## Présentation du langage

### Historique et généralités

- How Rust went from a side project to the world's most-loved programming language | MIT Technology Review, <https://www.technologyreview.com/2023/02/14/1067869/rust-worlds-fastest-growing-programming-language/> (Consulté en mars 2023)

### Caractéristiques du Rust

- Rust and C++ on Floating-point Intensive Code,
<https://www.reidatcheson.com/hpc/architecture/performance/rust/c++/2019/10/19/measure-cache.html> (Consulté en mars 2023)

## Innovations apportées par le Rust

### Possession et emprunt des variables

#### Mutabilité de variables

- The Rust Programming Language: Variables and Mutability,
<https://doc.rust-lang.org/book/ch03-01-variables-and-mutability.html> (Consulté en mars 2023)

#### Possession de variables

- The Rust Programming Language: What Is Ownership?,
<https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html> (Consulté en mars 2023)

#### Emprunt de variables

- The Rust Programming Language: References and Borrowing,
<https://doc.rust-lang.org/book/ch04-02-references-and-borrowing.html> (Consulté en mars 2023)

### Autres innovations

- Option - Rust By Example
<https://doc.rust-lang.org/rust-by-example/std/option.html> (Consulté en mars 2023)

- Result - Rust By Example
<https://doc.rust-lang.org/rust-by-example/std/result.html> (Consulté en mars 2023)

## Écosystème et applications

### La communauté Rust

#### Gouvernance du projet 

- Governance - Rust Programming Language,
<https://www.rust-lang.org/governance> (Consulté en mars 2023)

#### Popularité et statistiques

- Stack Overflow Developer Survey 2021,
<https://insights.stackoverflow.com/survey/2021> (Consulté en mars 2023)

- Stack Overflow Developer Survey 2022,
<https://survey.stackoverflow.co/2022/> (Consulté en mars 2023)

- Rust Breaks into TIOBE Top 20 Most Popular Programming Languages, <https://www.infoq.com/news/2020/06/rust-top-20-language/> (Consulté en mars 2023)

### L'environnement de développement

#### Les crates

- crates.io: Rust Package Registry,
<https://crates.io/> (Consulté en mars 2023)

#### Cargo

- Introduction - The Cargo Book,
<https://doc.rust-lang.org/cargo/> (Consulté en mars 2023)

#### Aides au développement

- Learn Rust - Rust Programming Language,
<https://www.rust-lang.org/learn> (Consulté en mars 2023)

### Applications

#### La programmation système

- redox-os / redox · GitLab,
<https://gitlab.redox-os.org/redox-os/redox> (Consulté en mars 2023)

- GitHub - helix-editor/helix: A post-modern modal text editor,
<https://github.com/helix-editor/helix> (Consulté en mars 2023)

#### Les interfaces graphiques

- Is Iced replacing GTK apps for the new COSMIC desktop? : pop os,
<https://www.reddit.com/r/pop_os/comments/xs87ed/comment/iqjc35b/?utm_source=reddit&utm_medium=web2x&context=3> (Consulté en mars 2023)

- iced-rs/iced: A cross-platform GUI library for Rust, inspired by Elm,
<https://github.com/iced-rs/iced> (Consulté en mars 2023)

#### Le *machine learning*

- A work-in-progress to catalog the state of machine learning in Rust,
<https://www.arewelearningyet.com/> (Consulté en mars 2023)

#### La programmation Web
