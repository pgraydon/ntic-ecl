# Summary

[Introduction](README.md)

- [Présentation du langage](presentation/README.md)
    - [Historique et généralités](presentation/language.md)
    - [Caractéristiques du Rust](presentation/characteristics.md)

- [Innovations apportées par le Rust](innovations/README.md)
    - [Possession et emprunt de variables](innovations/borrowing-ownership/borrowing-ownership.md)
        - [Mutabilité de variables](innovations/borrowing-ownership/mutability.md)
        - [Possession de variables](innovations/borrowing-ownership/ownership.md)
        - [Emprunt de variables](innovations/borrowing-ownership/borrowing.md)
    - [Durée de vie de références](innovations/lifetimes/lifetimes.md)
    - [Autres innovations](innovations/others/others.md)

- [Écosystème et applications](ecosystem/README.md)
    - [La communauté Rust](ecosystem/community/community.md)
        - [Gouvernance du projet](ecosystem/community/governance.md)
        - [Popularité et statistiques](ecosystem/community/statistics.md)
    - [L'environnement de développement](ecosystem/environment/environment.md)
        - [Les *crates*](ecosystem/environment/crates.md)
        - [Cargo](ecosystem/environment/cargo.md)
        - [Aides au développement](ecosystem/environment/development.md)
    - [Applications](ecosystem/applications/applications.md)
        - [La programmation système](ecosystem/applications/systems.md)
        - [Les interfaces graphiques](ecosystem/applications/interfaces.md)
        - [Le *machine learning*](ecosystem/applications/machine-learning.md)
        - [La programmation Web](ecosystem/applications/web-programming.md)


-----

[Références](./references.md)
