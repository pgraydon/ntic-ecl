# Historique et généralités

Le langage Rust est né en tant que projet personnel d'un employé de Mozilla, Graydon Hoare, aux alentours de 2006. Après quelques années, en 2009, l'entreprise a pris note de ce projet et a commencé à y participer, le transformant ainsi en projet *open source*, avec l'intention de l'utiliser pour un moteur de navigateur Web expérimental appelé Servo (qui a été abandonné par la suite).

Après avoir officiellement révélé le langage et Servo en 2010, Mozilla et Graydon Hoare ont continué le développement du Rust, réussissant à finaliser une version fonctionnelle du compilateur officiel `rustc` en 2011.

En janvier 2012, est publiée la première version alpha du langage, suivi de la première version stable 1.0 en 2015.

En proposant des fonctionnalités et innovations suprenantes, notamment une forte sécurité de la mémoire, le Rust s'est fait remarquer assez rapidement, et s'est fait comparer aux langages performants et bas-niveau comme le C et le C++.

Sa réputation et sa popularité s'est depuis solidifiée lentement mais sûrement, donnant lieu à un écosystème aujourd'hui en plein essor, de plus en plus utilisé par des entreprises, qui vont des géants de la *tech* à ceux des industries automobile et même aérospatiale, par des *startups*, ainsi que pour des projets *open source*.

Certains prédisent qu'il sera un jour "présent partout", remplacant les vieilles bases de code écrites en C et en C++ vulnérables et non sécurisées.

-----

Contrairement à ce que l'on pourrait penser, le nom du langage ne vient pas de la rouille, mais d'une espèce de champignon appelé [*Pucciniales*](https://en.wikipedia.org/wiki/Rust_(fungus)), choisie par Graydon Hoare pour leur robustesse.
