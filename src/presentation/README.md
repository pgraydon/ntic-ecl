# Présentation du langage

Commencons par la première des questions : **qu'est-ce que le Rust ?**

L'objectif de cette partie est de répondre à cette question, en effectuant une présentation générale du langage, afin de se familiariser avec et de le situer dans l'univers des langages de programmation.

Après un bref historique, seront présentées ses caractéristiques par rapport à d'autres langages, ce qui comprendra notamment des *benchmarks* de performance.
