# Caractéristiques du Rust

Maintenant que son historique est établi, plongeons-nous dans une description plus technique de ses caractéristiques, en les comparant à celles des autres langages de programmation.

## Type de langage de programmation

Comme il a été évoqué précédemment, le Rust est un langage de programmation...

- Complètement libre et *open source*. L'entièreté de son code source peut être trouvé sur [la page GitHub du langage](https://github.com/rust-lang/rust).
- **Compilé** et non interprété. Il s'apparente en ce sens aux langages comme le C, le C++, le Java, ou encore le Go, et s'éloigne des langages comme le Python ou le JavaScript.
- **À usage général**. Il s'apparente en ce sens à tous les langages ci-dessus, et s'éloigne des langages comme SQL ou Matlab.
- **Statiquement typé**. Les variables ont toutes un type fixé lors de leur déclaration, et ne peuvent pas en changer par la suite, ce qui se rapproche du C, du C++, du Java, et s'éloigne du Python et du JavaScript.
- **À haut niveau d'abstraction**. Il s'apparente ici aux langages comme le Python et le JavaScript, et s'éloigne de ceux comme le C et le C++, qui restent très bas niveau et proches du *hardware*. Ceci peut paraître suprenant, surtout quand la performance du Rust est très similaire à celle du C++, comme nous allons le voir par la suite.

    Ceci s'explique principalement à travers les abstractions "à coût zéro", qui lui permettent de proposer de puissantes ergonomies syntaxiques tout en gardant une excellente performance. Ces abstractions seront également expliquées plus en détail par la suite.


## Compilation et performance

La compilation du Rust se fait directement en *machine code*, de manière similaire au C et au C++. Une des versions antérieures du compilateur Rust se basait d'ailleurs sur LLVM, également utilisé pour ces deux langages.

De plus, il n'y a pas de *garbage collector*, un programme annexe qui s'exécute en parallèle du code afin de gérer la mémoire qui lui est allouée et de libérer les blocs non utilisés. Un tel programme a tendance à être gourmand en ressources et à alourdir l'exécution des programmes des langages comme le Python et le JavaScript, diminuant ainsi leur performance.

Ces deux caractéristiques font que le Rust a une très bonne performance, du même ordre de grandeur que celle du C++. Pour illustrer ceci, la figure ci-dessous montre le résultat d'un benchmark comparant ces deux langages sur une tâche de calcul intense impliquant des opérations à virgule flottante dans des tableaux de données de taille variable (axe des abscisses).

![Graphique de résultat du *benchmark* de comparaison](../images/RustCPPBenchmark.png)
*Graphique du résultat d'un benchmark comparant les performances du Rust et du C++*

Ainsi, bien que le Rust optimisé soit légèrement plus lent que du C++ optimisé (courbes rouge et bleue), il rivalise sans peine avec du C++ standard (courbe violette).

Un facteur à prendre en compte est que le C++ est un langage bien plus vieux et mature : ses divers compilateurs ont pu être grandement optimisés au fil des années, alors que le Rust n'est que dans sa phase adolescente. Nous pouvons conclure que la performance du Rust est tout à fait honorable.

## Syntaxe du langage

La syntaxe du Rust ne propose rien de fondamentalement nouveau. Elle se rapproche de celle du JavaScript, du Java, du C ou du C++.

Prenons un exemple : le bloc de code ci-dessous définit un `struct` appelé `Vector`, sous forme d'un tuple de trois coordonnées du type `f32`, qui est un `float` de 32 bits. Le bloc `impl Vector` définit ensuite des méthodes publiques `new` et `dot`, représentant respectivement un genre de constructeur et un produit scalaire. Enfin la fonction `main`, obligatoire dans tout projet Rust de type `binary` (destiné à devenir un programme exécutable), initialise deux vecteurs, calcule leur produit scalaire et l'imprime à l'aide de la macro `println!`.

```rust
pub struct Vector(pub f32, pub f32, pub f32);

impl Vector {
    pub fn new(x: f32, y: f32, z: f32) -> Vector {
        Vector(x, y, z)
    }

    pub fn dot(&self, other: &Vector) -> f32 {
        (self.0 * other.0) + (self.1 * other.1) + (self.2 * other.2)
    }
}

fn main() {
    let vec1 = Vector::new(1.6, 5.5, 3.14);
    let vec2 = Vector::new(0.0, -2.4, 6.7);
    let vec_dot = vec1.dot(&vec2);
    println!("{}", vec_dot);
}
```

Quelques particularités sont tout de même à noter :
- Toute instruction se termine par un point-virgule, similairement à beaucoup d'autres langages. Cependant, il est possible de l'omettre dans la dernière instruction d'une fonction afin de renvoyer un résultat. Dans les cas où un résultat doit être renvoyé dans une étape intermédiaire, le mot-clé `return` est disponible, et doit être utilisé avec un point-virgule.
- Comme montré dans la fonction `main`, le type d'une variable peut être inféré par le compilateur dans les cas triviaux, ce qui ne nécessite pas d'annotation explicite. Dans le cas d'une annotation explicite, la syntaxe de déclaration d'une variable devient :

    `let vec3 : Vector = Vector::new(...);`
- Le type de valeur renvoyée par une fonction doit être explicitement mentionné dans sa signature, comme le montre le code ci-dessus. Les fonctions qui ne renvoient rien ont une signature similaire à celle de `main`.

    Il s'agit d'ailleurs d'une des règles qui permettent au compilateur d'inférer le type des variables.

## Abstractions "à coût zéro"

Les abstractions dites *à coût zéro* sont une particularité intéressante du Rust. Comme expliqué précédemment, elles lui permettent de proposer des ergonomies de syntaxe qui se rapprochent de celles disponibles en Python, tout en gardant une performance proche de celle du C++.

Prenons l'exemple des itérateurs, présents en Python et également en Rust. Les deux fonctions dans le bloc de code ci-dessous effectuent la même chose, c'est-à-dire la somme des carrés des entiers entre 1 et 100000. Le temps d'exécution de chaque fonction est mesuré et imprimé en microsecondes.

```rust
use std::time::Instant;

fn sum_sq_naive() -> i64 {
    let mut count = 0;
    for i in 1..=100000 {
       count += i * i; 
    }
    count
}

fn sum_sq_iter() -> i64 {
    (1..=100000).map(|x| x * x).sum()
}

fn main() {
    let start_naive = Instant::now();
    let naive_total = sum_sq_naive();
    let end_naive = Instant::now();
    println!("{}", naive_total);
    println!("Naive compute time: {} us", (end_naive - start_naive).as_micros());

    let start_iter = Instant::now();
    let iter_total = sum_sq_iter();
    let end_iter = Instant::now();
    println!("{}", iter_total);
    println!("Iterator compute time: {} us", (end_iter - start_iter).as_micros());
}
```

La première fonction effectue ce calcul naïvement à l'aide d'une boucle `for`, tandis que la deuxième effectue le calcul à l'aide des méthodes existantes sur des itérateurs. Celle-ci tient en une ligne, et il est beaucoup plus facile de comprendre ce que fait le code par rapport à la version naïve.

Bien que les résultats peuvent varier, ce qu'il faut retenir est que le temps d'exécution des deux fonctions est du même ordre du grandeur. Il y a des cas où une méthode de calcul est plus rapide que l'autre et vice versa, mais la syntaxe préférée en Rust est généralement la deuxième, pour les raisons venant d'être citées.

La raison pour laquelle ces abstractions offrent une perte de performance minime est que le compilateur arrive à réduire ces instructions à du *machine code* très similaire à celui du cas naïf.

## Sécurité

Comme décrit dans l'introduction, Rust est un langage avec une conception et plusieurs mécanismes uniques parmi les langages de programmation : ceux-ci sont le produit d'une réflexion poussée par ses concepteurs afin de résoudre des problèmes présents dans la plupart des autres langages.

Le problème le plus saillant, et celui pour lequel le Rust est le plus connu, est celui de la sécurité de la mémoire. C'est un véritable problème dans des langages comme le C et le C++, qui exigent une gestion manuelle de la mémoire, et où il est facile de "se tirer une balle dans le pied", comme les développeurs le disent souvent.

Rust parvient à résoudre ce problème en garantissant la sécurité de la mémoire, qui en conséquence garantit la sécurité du parallélisme : il n'est pas possible d'accéder à des espaces mémoire non alloués à l'exécution du programme, et la grande plupart des problèmes liés au parallélisme, comme les *data races*, disparaissent.

Cela a pour effet de rendre les programmes développés en Rust particulièrement robustes lorsqu'ils sont déployés : comme toutes les vérifications du code sont faites lors de la compilation, le programme qui en résulte est certain d'être sûr et fiables.

Les mécanismes innovants qui permettent ces garanties seront vues dans la partie suivante.


## Inconvénients du langage

Après avoir présenté le Rust et tous ses avantages, il convient de parler également de ses inconvénients.

Bien que le langage propose des mécanismes qui assurent la sécurité et la fiabilité du code lors de la compilation, il en résulte que le compilateur est un des plus stricts qui soient. Si les règles ne sont pas respectées à 100%, il refusera tout simplement votre code et ne le compilera pas.

Comme ces mécanismes sont également assez uniques dans l'univers des langages de programmation, il faut un certain temps pour s'y adapter et les intégrer. Cela fait donc du Rust **un des langages les plus difficiles à apprendre**, avec une expérience assez démoralisante et rude pour les débutants.

Cependant, cette difficulté ne vient pas sans bonne raison : le temps supplémentaire passé lors du développement du code représente d'autant plus de temps économisé à le maintenir et le réparer lorsqu'il sera déployé.

Enfin, même s'il est impitoyable et vous signalera souvent des erreurs, le compilateur a le grand avantage de fournir du *feedback* très précis et détaillé à propos de ces erreurs, étant capable de tracer leur source jusqu'au caractère près. Il donne même, quand il le peut, des conseils et des méthodes qui permettent de corriger votre code et de le rendre valable. Plusieurs exemples de ce *feedback* seront vus dans la suite.

Cela résulte ainsi en une expérience de développment certes difficile, mais toute de même agréable et *très* formatrice. La plupart des développeurs ayant fait cette expérience affirment en avoir beaucoup appris, et ont même pu utiliser ces pratiques de développement bénéfiques dans d'autres langages.
