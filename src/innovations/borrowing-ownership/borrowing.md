# Emprunt de variables

Continuons la métaphore du stylo : au lieu le donner à quelqu'un d'autre, et de ne plus pouvoir l'utiliser après, il vous est possible de simplement prêter votre stylo et de le récupérer ensuite.

Ceci se traduit en Rust par **l'emprunt**, qui intervient lorsqu'on ne veut pas transférer la possession d'une valeur à une autre partie du code, afin de pouvoir continuer à l'utiliser.

Il existe deux types d'emprunt : les **emprunts non mutables** et les **emprunts mutables**. C'est ici que nous retrouvons la notion de mutabilité décrite précédemment.

## Emprunt non mutable

Le premier type d'emprunt est l'emprunt non mutable. Comme son nom l'indique, il permet d'accéder à une variable **en lecture seule** et non en écriture. La variable elle-même peut être mutable ou non.

Pour pouvoir emprunter la valeur d'une variable au lieu d'en prendre la possession, il suffit de préfixer l'appel de la variable par une *référence*, notée `&`, qui est une forme de pointeur.

Ainsi, au lieu de consommer la valeur-même de la variable, les références permettent de consommer uniquement une instance "jetable" de l'adresse mémoire de la variable, qu'il est possible de regénérer autant de fois que souhaité à l'aide de nouvelles références.

Le code ci-dessous donne un exemple d'emprunt non mutable : au lieu de passer la variable `s1` telle quelle dans la fonction `calculate_length`, qui n'a pas besoin de prendre la possession de la valeur du `String` pour effectuer un simple calcul de longueur, il lui est passé une simple référence `&s1`.

Comme `s1` reste propriétaire de sa valeur après l'emprunt, il est toujours possible de l'utiliser ensuite, comme le montre l'instruction `println!`. Ce bloc de code est donc valable et peut être exécuté sans problème.
 

```rust
fn main() {
    let s1 = String::from("hello");

    // Here, s1 is only borrowed through a reference
    let len = calculate_length(&s1);

    // We can still use it afterwards
    println!("The length of '{}' is {}.", s1, len);
}

fn calculate_length(s: &String) -> usize {
    s.len()
}
```

## Emprunt mutable

Le deuxième type d'emprunt est l'emprunt mutable. Contrairement au premier type, il ne peut être effectué que sur une variable mutable, et constitue en pratique un accès **en écriture** à la variable.

Pour effectuer un emprunt mutable d'une variable mutable `s1`, il suffit d'écrire `&mut s1`, afin d'indiquer que l'emprunt est lui-même mutable.

Dans le code ci-dessous, la fonction `change` effectue une opération qui modifie la variable en entrée, à l'aide de la fonction `push_str`. Or la référence faite à s1, et la référence en paramètre de la fonction `change` ne sont pas mutables : ce code ne compilera donc pas, et le compilateur renverra une erreur.

```rust
fn main() {
    let mut s = String::from("hello");

    // Here, s is only borrowed through a reference
    change(&s);

    // We can still use it afterwards
    println!("{}", s);
}

fn change(some_string: &String) {
    some_string.push_str(", world");
}
```

En tentant de compiler ce code, nous voyons que le compilateur refuse bel et bien, et nous indique qu'il faudrait effectivement effectuer une référence mutable afin de corriger ce code.

Une fois la modification faite, le code s'exécute sans problème, et nous imprime la chaîne de caractères modifiée.

```rust
fn main() {
    let mut s = String::from("hello");
    
    // Here, s is mutably borrowed through a reference
    change(&mut s);

    // We can still use it afterwards
    println!("{}", s);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}
```

## Règles d'emprunt des variables

Ces deux types d'emprunt permettent ainsi d'accéder à la valeur d'une variable sans en prendre possession, afin de pouvoir la réutiliser dans la suite du code.

Cependant, toujours dans un souci de sécurité des données, une règle importante régule leur utilisation, qui est vérifiée par le compilateur lors de la compilation. Cette règle est la suivante :

- Il est possible d'effectuer simultanément un nombre arbitraire d'emprunts non mutables sur une variable, **OU** un seul emprunt mutable.

Vous avez peut-être remarqué que cette règle s'apparente aux règles d'accès à une base de données : des accès libres en lecture, ou une seule en écriture... Cette similarité n'est en effet pas anodine, puisqu'elle permet notamment d'éviter des problèmes liés à l'écriture en parallèle sur les variables.

-----

Ainsi, le mécanisme de possession et d'emprunt des variables offre deux avantages, à travers un fonctionnement relativement simple :

- Il contribue à la gestion automatique et sécurisée de la mémoire
- Il contribue également, par conséquent, à la fiabilité de la programmation concurrente et parallèle.
