# Mutabilité de variables

Avant de plonger dans le détail de la possession et de l'emprunt, il faut comprendre une notion clé, qui est une particularité du Rust : la **mutabilité**.

En Rust, toute variable initialisée est immuable par défaut, sauf si la déclaration est affixée d'un mot clé dédié `mut`.

Voyons cela dans un exemple : le bloc de code ci-dessous déclare simplement une variable `x` et tente de modifier sa valeur. Si vous tentez de compiler ce code, le compilateur refusera et vous expliquera pourquoi en détail.

```rust
fn main() {
    let x = 5;
    println!("The value of x is: {x}");
    x = 6;
    println!("The value of x is: {x}");
}
```

Effectivement, le compilateur indique que la variable est immuable. Mais comme il est notre ami, il nous suggère d'insérer le mot-clé `mut` dans la déclaration de `x`, afin de rendre le code fonctionnel.

C'est ce qui est fait dans le bloc de code ci-dessous, qui exécutera avec succès et imprimera les valeurs souhaitées.

```rust
fn main() {
    let mut x = 5;
    println!("The value of x is: {x}");
    x = 6;
    println!("The value of x is: {x}");
}
```
