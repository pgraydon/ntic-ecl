# Possession et emprunt de variables

Au lieu d'imposer une gestion manuelle et explicite de la mémoire, ou d'avoir un *garbage collector* qui exécute parallèlement à un programme afin d'effectuer cette gestion automatiquement, le Rust propose une troisième approche : **la possession et l'emprunt de variables**.

Simplement appelée *ownership* en anglais, il s'agit d'un système de gestion de la mémoire gouverné par un ensemble de règles. Tout programme doit respecter ces règles afin d'être valable et accepté par le compilateur.

C'est un mécanisme tout à fait unique parmi les langages de programmation, et qui demande donc un certain temps d'adaptation. Cependant, il assure presque à lui seul que tout programme valable en Rust sera sécurisé et fiable.

Comme ces règles dépendent du type de donnée manipulée, il ne sera expliqué que les grands principes du système : le chapitre dédié du guide officiel, [Understanding Ownership](https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html), rentre dans l'ensemble des détails pour ceux qui souhaiteraient approfondir le sujet.
