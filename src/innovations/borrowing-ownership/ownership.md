# Possession de variables

Comme indiqué dans son nom, la première partie du système de possession et d'emprunt est **la posession**.

Commencons par un exemple illustratif : imaginez-vous avec un simple stylo. Il peut être affirmé que vous *possédez* ce stylo : il appartient à vous et à personne d'autre.
De plus, comme toute possession, il est possible de le donner à quelqu'un d'autre lorsque vous n'en avez plus besoin. Lorsque vous faites cela, le stylo est irrécupérable ; vous ne pouvez plus l'utiliser après l'avoir donné.

Lorsqu'on transpose cette image à celle d'une variable et de la valeur qui lui est affectée, nous obtenons essentiellement le concept de possession avec lequel le Rust opère : toute variable *possède* la valeur qui lui est affectée, et peut transférer cette possession à d'autres parties du programme comme une autre variable ou une fonction.

Prenons un exemple de code concret. Dans le bloc de code ci-dessous, la variable `s` est initialisée avec la valeur `String::from("Hello")` : `s` possède donc cette valeur. Ensuite, `s` est passée en argument de la fonction `takes_ownership`, avec comme type d'argument un `String`. Lors de cet appel, la valeur de `s` est transférée à la fonction, qui en prend donc la possession à son tour : toute tentative d'accès à la valeur de `s` après cet appel est interdite, puisqu'elle n'est plus là.

L'instruction `println!("{}", s);` viole donc les règles de possession, et le compilateur refusera le code. Si vous tentez d'ailleurs la compilation de ce code, le compilateur vous donnera la même explication que ci-dessus. Il suggère également de corriger le code en effectuant un *borrow*, ou emprunt, ce qui nous mène à la partie suivante du système sur l'emprunt des variables.

```rust
fn main() {
    let s = String::from("Hello");

    takes_ownership(s);             // s's value moves into the function...
                                    // ... and so is no longer valid here

    println!("{}", s);              // Using s here is not allowed
}

fn takes_ownership(some_string: String) {
    println!("{}", some_string);
}
```
