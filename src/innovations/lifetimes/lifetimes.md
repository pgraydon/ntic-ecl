# Durée de vie de références

Bien que le principe de possession et d'emprunt des variables contribue grandement à la sécurité de la mémoire, il s'avère qu'il n'est pas suffisant à lui seul.

En effet, un problème majeur qu'il ne résout pas est celui des *dangling pointers* : des pointeurs, ou références dans notre cas, qui pointent vers une donnée qui n'existe plus en mémoire. Cet espace mémoire peut alors être non alloué ou contenir une autre donnée, ce qui est non sécurisé et peut être dangereux.

C'est ici qu'intervient la deuxième grande innovation du Rust : les **durées de vie**, qui sert majoritairement à résoudre ce problème. Il s'agit d'un concept qui n'est même pas utilisé par la grande plupart des autres langages de programmation, et qui demande donc un certain temps d'adaptation.

Comme il est également assez complexe, seuls les fondamentaux de ce mécanisme seront présentés ici. Les lecteurs curieux qui voudraient en savoir plus peuvent se référer au chapitre [*Validating References with Lifetimes*](https://doc.rust-lang.org/book/ch10-03-lifetime-syntax.html) du guide officiel.

L'idée fondamentale est qu'à toute référence en Rust est associée une certaine *durée de vie*, qui est simplement la portée du code au sein de laquelle la référence est valable. À l'aide de règles précises, le compilateur est capable d'inférer ces durées de vie lui-même, de manière similaire à l'inférence du type d'une variable.

Cependant, dans certains cas, le compilateur ne peut calculer ces durées de vie, qui doivent donc être annotées à la main : le développeur assure ainsi que les références dans le code resteront valables, et pointeront vers les bonnes données.

Prenons de suite un exemple. Le code ci-dessous cherche simplement à déterminer la chaîne de caractères la plus longue, à l'aide d'une fonction qui prend comme paramètres les chaînes à comparer, qui sont du type `&str` qui utilise une référence.

Cela paraît assez simple, mais ce code ne compilera pas, puisque le compilateur n'arrive pas à déterminer si la référence renvoyée en sortie correspond à `x` ou à `y` : le bloc `if` renvoie l'un, tandis que le bloc `else` renvoie l'autre. De plus, le compilateur ne peut déterminer la relation entre les durées de vie de `x` et de `y`, et la durée de vie de la référence en sortie.

```rust
fn main() {
    let string1 = "abcd";
    let string2 = "xyz";

    let result = longest(string1, string2);
    println!("The longest string is {}", result);
}

fn longest(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

Si nous tentons d'exécuter ce bloc de code, le compilateur nous donnera cette explication, et nous indiquera qu'il faut annoter les durées de vie à la main, en nous donnant la syntaxe à utiliser. Cela donne le code corrigé suivant, qui utilise un *paramètre générique de durée de vie* précisant la relation entre les durées de vie de différentes références :

```rust
fn main() {
    let string1 = "abcd";
    let string2 = "xyz";

    let result = longest(string1, string2);
    println!("The longest string is {}", result);
}

fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

La signature de la fonction peut être interprétée de la manière suivante : pour une certaine durée de vie `'a`, la fonction `longest` prend en paramètres deux références avec **au moins** la durée de vie `'a`, et renvoie une référence avec **au moins** la durée de vie `'a`.

En d'autres termes, la référence de sortie a une durée de vie égale au minimum des durées de vie de `x` et de `y`, ce qui garantit que la référence de sortie pointe vers une donnée valide : ici, `x` ou `y`.

Maintenant que le compilateur sait que la référence reste valable et ne deviendra pas une *dangling reference*, il accepte le code ci-dessus, qui renverra donc le bon résultat.

-----

Ainsi, c'est ce mécanisme des durées de vie, associé à celui de la possession et de l'emprunt qui permettent au Rust de gérer la mémoire des programmes de manière sécurisée et fiable, et de se passer entièrement d'un *garbage collector* gourmand en ressources.
