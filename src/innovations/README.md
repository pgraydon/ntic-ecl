# Innovations apportées par le Rust

Nous avons vu qu'un des atouts principaux du Rust est la garantie que tout code compilé sera sécurisé et fiable du point de vue des accès mémoire.

Il arrive à fournir ces garanties grâce à deux mécanismes majeurs au coeur du langage, qui permettent une gestion automatisée de la mémoire tout en minimisant les vulnérabilités : 
- **la possession et l'emprunt de variables**
- **la durée de vie de références**

Ce sont ces deux mécanismes innovants qui seront donc présentés ici, avec une courte extension sur d'autres innovations plus mineures mais néanmoins intéressantes.
