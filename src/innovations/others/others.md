# Autres innovations

Enfin, pour clore cette partie sur des concepts plus sympathiques, présentons rapidement quelques autres innovations mineures qui contribuent à rendre le Rust sûr et fiable.

## Gestion des erreurs

Une particularité du langage, que vous rencontrerez rapidement si vous en faites, est qu'il force la gestion des erreurs.

En effet, dès qu'une fonction peut ne rien renvoyer, ou renvoyer une erreur, alors le résultat de la fonction sera donné sous forme d'un des deux `enum` suivants :

- Lorsqu'une partie d'un programme peut ne rien renvoyer à la place de la valeur souhaitée, le résultat est "emballé" dans un `enum` `Option<T>`. Celui-ci a deux variantes : `Some(T)`, qui contient la valeur du type `T` souhaitée, ou `None`, qui est une valeur qui ne représente rien. 

    Par exemple, voici une fonction qui effectue une division de deux entiers, où `None` est renvoyé dans le cas où le diviseur vaut 0 :

    ```rust
    // An integer division that doesn't `panic!`
    fn checked_division(dividend: i32, divisor: i32) -> Option<i32> {
        if divisor == 0 {
            // Failure is represented as the `None` variant
            None
        } else {
            // Result is wrapped in a `Some` variant
            Some(dividend / divisor)
        }
    }
    ```

- Dans les cas plus sévères qui exigent le renvoi d'une erreur, c'est `Result<T,E>` qui est utilisé, qui a également deux variantes : la première est `Ok(T)`, qui est similaire à `Some(T)` ci-dessus et qui contient la valeur souhaitée. La deuxième est `Err(E)`, qui contient une erreur de type `E` qui devra donc être traitée.

    De manière similaire à ci-dessus, il est possible de définir des fonctions mathématiques qui génèrent une erreur lorsqu'elles échouent :

    ```rust
    pub enum MathError {
            DivisionByZero,
            NegativeSquareRoot,
        }

    pub type MathResult = Result<f64, MathError>;

    pub fn div(x: f64, y: f64) -> MathResult {
        if y == 0.0 {
            // This operation would `fail`, instead let's return the reason of
            // the failure wrapped in `Err`
            Err(MathError::DivisionByZero)
        } else {
            // This operation is valid, return the result wrapped in `Ok`
            Ok(x / y)
        }
    }

    pub fn sqrt(x: f64) -> MathResult {
        if x < 0.0 {
            Err(MathError::NegativeSquareRoot)
        } else {
            Ok(x.sqrt())
        }
    }
    ```

Si nous voulons ainsi extraire la valeur souhaitée de l'`enum`, il nous faut également gérer le cas où celui-ci ne la contient pas, ce qui force bel et bien la gestion des erreurs.

Bien que cela puisse sembler lourd et pénible, cette manière de fonctionner fait que cette gestion des erreurs est assistée par le compilateur lui-même, qui nous indiquera si jamais un cas a été oublié. Elle résulte ainsi en du code fiable, où un maximum d'erreurs sont considérées et prévisibles, ce qui évite ainsi bien des maux de tête lorsque le code est déployé.
