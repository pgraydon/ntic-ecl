# Le langage de programmation Rust : innovations et applications

Vous en avez sûrement entendu parler quelques fois au tournant d'une conversation ou sur Internet, sans jamais trop vous attarder dessus.

Encore assez jeune et relativement méconnu, et n'étant qu'un langage de programmation parmi tant d'autres, Rust a pourtant des atouts à faire valoir. Ses créateurs ont cherché à résoudre des problèmes répandus dans l'univers des langages de programmation :
- Comment avoir un langage avec l'ergonomie de syntaxe du JavaScript, *et* avec la performance du C++, une combinaison jusqu'alors considérée impensable ?
- Comment avoir un langage qui garantit la sécurité de la mémoire et la fiabilité du parallélisme, mettant fin au fameux *segfault* qui fait pleurer tant de développeurs ?
- Comment améliorer l'expérience générale de développement, avec par exemple un *feedback* détaillé des erreurs rencontrées par le compilateur ? 

Le langage Rust est le résultat de ce travail continu. Grâce à ses nombreuses innovations réfléchies, il est aujourd'hui en plein essor, et devient de plus en plus utilisé, que ce soit dans le monde professionnel ou dans l'*open source*. Certains osent même l'appeler le "tueur du C++"...

Cette étude vise ainsi à présenter ce langage moderne, et lui apporter un peu d'attention méritée. Après un bref historique et une description générale de ses caractéristiques, seront expliqués plus en détail ses deux innovations majeures, qui lui permettent entre autres de garantir la sécurité de la mémoire et du parallélisme. Enfin, il sera fait état de son écosystème et de ses diverses applications.

-----

Ce site a été généré avec le programme utilitaire [`mdBook`](https://rust-lang.github.io/mdBook/), qui permet de créer de la documentation sous forme d'un livre en ligne à partir de fichiers Markdown.

Il est bien sûr écrit en Rust, et a d'ailleurs été créé par ses développeurs, afin de pouvoir publier le guide officiel du langage, [The Rust Programming Language](https://doc.rust-lang.org/book/), gratuitement en ligne.

Une particularité de `mdBook` est de pouvoir rendre les blocs de code Rust intégrés dans le texte exécutables : vous êtes donc invités à le faire lorsque vous en rencontrez, le code servant à complémenter et à illustrer les propos discutés !
